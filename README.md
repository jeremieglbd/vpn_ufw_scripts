Killswitch for openvpn using ufw rules.

`firewall.sh` sets the killswitch.
Make sure the vpn is connected before executing the script.

Once the killswitch is no longer useful or the connection to
the vpn has been lost, launch `unfirewall.sh`.

Install ufw with :

``` bash
sudo dnf install ufw
```

Originaly found at [The Tin Hat](https://thetinhat.com/tutorials/misc/linux-vpn-drop-protection-firewall.html)
